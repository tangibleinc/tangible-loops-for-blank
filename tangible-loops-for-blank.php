<?php
/**
 * Plugin Name: Tangible: Loops for Blank
 * Plugin URI: https://tangibleplugins.com/tangible-loops-for-blank
 * Description: Loop types, fields, and conditions for Blank
 * Version: 0.0.1
 * Author: Team Tangible
 * Author URI: https://teamtangible.com
 * License: GPLv2 or later
 */

define( 'TANGIBLE_LOOPS_FOR_BLANK_VERSION', '0.0.1' );

require __DIR__ . '/vendor/tangible/plugin-framework/index.php';

/**
 * Get plugin instance
 */
function tangible_loops_for_blank($instance = false) {
  static $plugin;
  return $plugin ? $plugin : ($plugin = $instance);
}

add_action('plugins_loaded', function() {

  $framework = tangible();
  $plugin    = $framework->register_plugin([
    'name'           => 'tangible-loops-for-blank',
    'title'          => 'Loops for blank',
    'setting_prefix' => 'tangible_loops_for_blank',

    'version'        => TANGIBLE_LOOPS_FOR_BLANK_VERSION,
    'file_path'      => __FILE__,
    'base_path'      => plugin_basename( __FILE__ ),
    'dir_path'       => plugin_dir_path( __FILE__ ),
    'url'            => plugins_url( '/', __FILE__ ),
    'assets_url'     => plugins_url( '/assets', __FILE__ ),

    // Product ID on Tangible Plugins store - See also package.json, property "productId"
    'item_id'        => false,

    'multisite'      => false,
  ]);

  $plugin->register_dependencies([
    'tangible-loops-and-logic/tangible-loops-and-logic.php' => [
      'title' => 'Tangible Loops & Logic',
      'url' => 'https://tangibleplugins.com/tangible-loops-and-logic',
      'fallback_check' => function() {
        return function_exists('tangible_loops_and_logic');
      }
    ],
    // 'example/example.php' => [
    //   'title' => 'Example dependency',
    //   'url' => 'https://example.com/',
    //   'fallback_check' => function() {
    //     return function_exists('example');
    //   }
    // ],
  ]);

  if ( ! $plugin->has_all_dependencies() ) return;

  tangible_loops_for_blank( $plugin );

  // Depends on Loops & Logic
  add_action('tangible_loops_and_logic_ready', function($loop, $logic, $html) use ($framework, $plugin) {

    // Features loaded will have in their scope: $framework, $plugin, ..

    require_once __DIR__.'/includes/index.php';

  }, 10, 3);

}, 10);

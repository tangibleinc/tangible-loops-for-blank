=== Tangible: Loops for Blank ===
Requires at least: 4.0
Tested up to: 5.4.2
Requires PHP: 7
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags:

Loop types, fields, and conditions for Blank

== Description ==

Loop types, fields, and conditions for Blank

== Installation ==

1. Install & activate in the admin: *Plugins -&gt; Add New -&gt; Upload Plugins*

== Changelog ==

= 0.0.1 =

Release Date: 2021-00-00

- Initial release

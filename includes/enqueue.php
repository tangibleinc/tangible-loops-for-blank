<?php
// local $plugin

// Enqueue frontend styles and scripts

add_action('wp_enqueue_scripts', function() use ($plugin) {

  $url = $plugin->url;
  $version = $plugin->version;

  wp_enqueue_style(
    'tangible-loops-for-blank',
    $url . 'assets/build/tangible-loops-for-blank.min.css',
    [],
    $version
  );

  wp_enqueue_script(
    'tangible-loops-for-blank',
    $url . 'assets/build/tangible-loops-for-blank.min.js',
    ['jquery'],
    $version
  );

});
